# Quick Install EJBCA CE 7.4.3.2 and SignServer CE 5.2.0

Spesifikasi server (dijalankan pada Virtual Machine)
* RAM = 6 GB
* CPUs = 6
* Hard disk = 50 GB

System Prerequisites
* EJBCA CE 7.4.3.2
* OS = CentOS 7
* Application Server = Wildfly-10.1.0.Final
* Database = MariaDB
* Java = OpenJDK 8
* Build Tool = Apache Ant

## 1. Konfigurasi Awal CentOS 7

`sudo yum update`<br/>
`sudo yum install vim ntp ntpdate epel-release wget tar unzip java-1.8.0.openjdk-devel git ant psmisc mariadb bc patch`<br/>
`sudo yum update install`<br/>

## 2. Konfigurasi Firewall

`sudo firewall-cmd --zone=public --permanent --add-port=80/tcp`<br/>
`sudo firewall-cmd --zone=public --permanent --add-port=443/tcp`<br/>
`sudo firewall-cmd --zone=public --permanent --add-port=8443/tcp`<br/>
`sudo firewall-cmd --zone=public --permanent --add-port=9990/tcp`<br/>
`sudo firewall-cmd --zone=public --permanent --add-port=8080/tcp`<br/>
`sudo firewall-cmd --zone=public --permanent --add-port=8442/tcp`<br/>
`sudo firewall-cmd --zone=public --permanent --add-port=4447/tcp`<br/>

## 3. Instalasi dan Konfigurasi Database MariaDB
### Instalasi Database MariaDB

`sudo yum install mariadb-server`<br/>
`sudo systemctl enable mariadb`<br/>
`sudo systemctl start mariadb`<br/>
`sudo systemctl status mariadb`<br/>
`sudo mysql_secure_installation`<br/>

### Konfigurasi Database MariaDB

`mysql -u root -p`<br/>
`mysql> CREATE DATABASE ejbcatest CHARACTER SET utf8 COLLATE utf8_general_ci;`<br/>
`mysql> GRANT ALL PRIVILEGES ON ejbcatest.* TO 'ejbca'@'localhost' IDENTIFIED BY 'ejbca';`<br/>

## 4. Instalasi EJBCA CE 7.4.3.2

Download ejbca dengan menjalankan perintah berikut ini:

`wget https://sourceforge.net/projects/ejbca/files/ejbca7/ejbca_ce_7_4_3_2.zip`<br/>
`unzip ejbca_ce_7_4_3_2.zip`<br/>

### Konfigurasi CA
Setelah archive ejbca berhasil di unpack, masuk ke direktori ejbca tersebut. Selanjutnya kita akan melakukan konfigurasi terhadap CA yang akan kita bangun. Jalankan perintah berikut ini:

`vi ejbca_ce_7_4_3_2/bin/extra/ejbca-setup.sh`<br/>

Atur beberapa variabel pada ejbca setup, minimal terdiri dari:
- BASE_DN
- ca_name
- ca_dn
- superadmin_password

Lainnya biarkan variabel diatur menggunakan konfigurasi default.

### Instalasi CA

Setelah ejbca_setup sudah diatur sesuai dengan kebutuhan, jalankan perintah berikut untuk melakukan instalasi:
`./ejbca_ce_7_4_3_2/bin/extra/ejbca-setup.sh`<br/>

Tunggu sampai proses instalasi selesai dilakukan.

Apabila proses instalasi selesai, coba untuk melakukan akses ke http://localhost:8080/ejbca. Agar dapat masuk ke halaman Administration Web, import file superadmin.p12 dan install pada web browser. Jika sudah, akses ke https://localhost:8443/ejbca/adminweb atau klik Administration pada halaman ejbca Public Web. Server akan meminta kita untuk memilih client certificate yang sudah diinstal sebelumnya.

## 5. Instalasi SignServer CE 5.2.0

Download SignServer dengan menjalankan perintah berikut ini:

`wget https://sourceforge.net/projects/signserver/files/signserver/5.2/signserver-ce-5.2.0.Final.zip`<br/>
`unzip signserver-ce-5.2.0.Final.zip`<br/>

### Konfigurasi Awal

Tambahkan konfigurasi `binlog_format=row` pada my.cnf, melalui perintah `vi /etc/my.cnf`

Tambahkan data source berikut, pada jboss_cli.sh, dengan masuk ke direktori `wildfly/bin` dan menjalankan perintah `./jboss_cli.sh -c`:

`data-source add --name=signserverds --driver-name="mariadb-java-client.jar" --connection-url="jdbc:mysql://127.0.0.1:3306/signserver" --jndi-name="java:/SignServerDS" --use-ccm=true --driver-class="org.mariadb.jdbc.Driver" --user-name="signserver" --password="signserver" --validate-on-match=true --background-validation=false --prepared-statements-cache-size=50 --share-prepared-statements=true --min-pool-size=5 --max-pool-size=150 --pool-prefill=true --transaction-isolation=TRANSACTION_READ_COMMITTED --check-valid-connection-sql="select 1;" --enabled=true`<br/>
`:reload`<br/>

### Instalasi SignServer

Untuk melakukan build SignServer, membutuhkan Maven. Install Maven dengan menjalankan perintah berikut:

`sudo yum install maven`<br/>

Berikut rangkaian perintah untuk melakukan build SignServer:
`mvn help:effective-settings -X`<br/>
`bin/ant init`<br/>
`mvn install -DskipTests -X`<br/>
`export APPSRV_HOME=/home/${user}/wildfly`<br/>
`export SIGNSERVER_NODEID=node1`<br/>
`cp conf/signserver_deploy.properties.sample conf/signserver_deploy.properties`<br/>
`vi conf/signserver_deploy.properties`<br/>
Aktifkan `database.name=mysql`<br/>
`mkdir -p ../signserver-custom/conf`<br/>
`cp conf/signserver_deploy.properties.sample ../signserver-custom/conf/signserver_deploy.properties`<br/>
`bin/ant deploy`<br/>
`bin/signserver wsadmins -allowany`<br/>

Setelah SignServer berhasil diinstall, silahkan akses http://localhost:8080/signserver.

